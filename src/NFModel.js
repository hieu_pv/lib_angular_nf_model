class NFModel {
    constructor(options) {
        this.raw = options;
        this.bind(options);
        if (this._hidden === undefined) {
            this._hidden = [];
        }
        if (this._transform !== undefined) {
            this._transform();
        }
    }
    bind(options) {
        for (let k in options) {
            if (this._hidden.indexOf(k) < 0) {
                this[k] = options[k];
            }
        }
    }
    getRaw() {
        return this.raw;
    }
    _get(key) {
        if (this._hidden.indexOf(key) < 0 && this[key] !== undefined) {
            return this[key];
        }
    }
}
