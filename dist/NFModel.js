"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var NFModel = function () {
    function NFModel(options) {
        _classCallCheck(this, NFModel);

        this.raw = options;
        this.bind(options);
        if (this._hidden === undefined) {
            this._hidden = [];
        }
        if (this._transform !== undefined) {
            this._transform();
        }
    }

    _createClass(NFModel, [{
        key: "bind",
        value: function bind(options) {
            for (var k in options) {
                if (this._hidden.indexOf(k) < 0) {
                    this[k] = options[k];
                }
            }
        }
    }, {
        key: "getRaw",
        value: function getRaw() {
            return this.raw;
        }
    }, {
        key: "_get",
        value: function _get(key) {
            if (this._hidden.indexOf(key) < 0 && this[key] !== undefined) {
                return this[key];
            }
        }
    }]);

    return NFModel;
}();