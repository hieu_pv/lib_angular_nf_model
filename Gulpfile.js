var gulp = require("gulp");
var babel = require("gulp-babel");
var watch = require('gulp-watch');
var jshint = require('gulp-jshint');
var browserify = require('gulp-browserify');

gulp.task("babel", function() {
    return gulp.src("src/*.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

gulp.task('watch', function() {
    watch(['./src/**/*.js'], function() {
        gulp.run([
            'jshint',
            'babel'
        ]);
    });
    watch(['./app/**/*.js'], function() {
        gulp.run([
            'jshint',
            'browserify'
        ]);
    });
});


gulp.task('jshint', function() {
    return gulp.src(['./src/**/*.js', './app/**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('default', function() {
    return gulp.run([
        'jshint',
        'babel',
        'browserify',
    ]);
})

gulp.task('browserify', function() {
    return gulp.src('./app/app.js')
        .pipe(browserify({
            insertGlobals: true
        }))
        .pipe(gulp.dest('./'));
});
